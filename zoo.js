var selectedItem;
var obj;


/*WildBeast Ptyperototype*/
function WildBeast() {
    this.feet = 4;
}
WildBeast.prototype.feedAnimal = function() {
    console.log('Ok, feed the wild beast.');
}

/*Bird Prototype*/
function Bird() {
    this.feet = 2;
}
Bird.prototype.feedAnimal = function() {
    console.log('Ok, feed the bird.');
}
Bird.prototype.feedAnimal = function() {
    console.log('Ok, feed the bird.');
}

/*Bird type Prototypes*/
function Parrot(length = 0, weight = 0, gender, age = 0) {
    Bird.call(this);
    this.length = length;
    this.weight = weight;
    this.gender = gender;
    this.age = age;
}
function Pigeon(length = 0, weight = 0, gender, age = 0) {
    Bird.call(this);
    this.length = length;
    this.weight = weight;
    this.gender = gender;
    this.age = age;
}
function Mynah(length = 0, weight = 0, gender, age = 0) {
    Bird.call(this);
    this.length = length;
    this.weight = weight;
    this.gender = gender;
    this.age = age;
}

Parrot.prototype = Object.create(Bird.prototype);
Pigeon.prototype = Object.create(Bird.prototype);
Mynah.prototype = Object.create(Bird.prototype);

Parrot.prototype.constructor = Parrot;
Pigeon.prototype.constructor = Pigeon;
Mynah.prototype.constructor = Mynah;

/*Wild type Prototypes*/
function Tiger(length = 0, weight = 0, gender, age = 0) {
    Bird.call(this);
    this.length = length;
    this.weight = weight;
    this.gender = gender;
    this.age = age;
}
function Lion(length = 0, weight = 0, gender, age = 0) {
    Bird.call(this);
    this.length = length;
    this.weight = weight;
    this.gender = gender;
    this.age = age;
}
function Chita(length = 0, weight = 0, gender, age = 0) {
    Bird.call(this);
    this.length = length;
    this.weight = weight;
    this.gender = gender;
    this.age = age;
}

Tiger.prototype = Object.create(WildBeast.prototype);
Lion.prototype = Object.create(WildBeast.prototype);
Chita.prototype = Object.create(WildBeast.prototype);

Tiger.prototype.constructor = Tiger;
Lion.prototype.constructor = Lion;
Chita.prototype.constructor = Chita;

/* Populating Data */
var parrotList = new Array(0);
var pigeonList = new Array(0);
var mynahList = new Array(0);
parrotList.push(new Parrot(13,1, 'm', 9));
parrotList.push(new Parrot(14,2, 'f', 10));
parrotList.push(new Parrot(15,3, 'f', 11));
parrotList.push(new Parrot(16,4, 'f', 12));

pigeonList.push(new Pigeon(17,5, 'm', 9));
pigeonList.push(new Pigeon(18,6, 'f', 10));
pigeonList.push(new Pigeon(19,7, 'f', 11));
pigeonList.push(new Pigeon(20,8, 'f', 12));

mynahList.push(new Mynah(21,9, 'm', 9));
mynahList.push(new Mynah(22,10, 'f', 10));
mynahList.push(new Mynah(23,11, 'f', 11));
mynahList.push(new Mynah(24,12, 'f', 12));

var tigerList = new Array(0);
var lionList = new Array(0);
var chitaList = new Array(0);
tigerList.push(new Tiger(1,1, 'm', 9));
tigerList.push(new Tiger(2,2, 'f', 10));
tigerList.push(new Tiger(3,3, 'f', 11));
tigerList.push(new Tiger(4,4, 'f', 12));

lionList.push(new Lion(5,5, 'm', 9));
lionList.push(new Lion(6,6, 'f', 10));
lionList.push(new Lion(7,7, 'f', 11));
lionList.push(new Lion(8,8, 'f', 12));

chitaList.push(new Chita(9,9, 'm', 9));
chitaList.push(new Chita(10,10, 'f', 10));
chitaList.push(new Chita(11,11, 'f', 11));
chitaList.push(new Chita(12,12, 'f', 12));

/* Working With DOM */
function wildBeastExpander() {
    var table = document.getElementsByClassName('menue')[0];
    var table2 = document.createElement('table');
    var inner = '';

    var att = document.createAttribute("class");
    att.value = "menue";
        table2.setAttributeNode(att);

    inner += '<tr><td onclick="showTigerList()">Tiger</td></tr>';
    inner += '<tr><td onclick="showLionList()">Lion</td></tr>';
    inner += '<tr><td onclick="showChitaList()">Chita</td></tr>';
    table2.innerHTML = inner;
    table.parentNode.replaceChild(table2,table);
}
function showTigerList() {
    obj = new Tiger();
    selectedItem = tigerList;
    showList(tigerList)
}
function showLionList() {
    obj = new Lion();
    selectedItem = lionList;
    showList(lionList)
}
function showChitaList() {
    obj = new Chita();
    selectedItem = chitaList;
    showList(chitaList)
}
function showList(list) {
    var main = document.getElementById('app-root');
    var table2 = document.createElement('table');
    var inner = '';

    var table1 = document.getElementsByClassName('animalList')[0];
    if(table1 !== undefined) {
        table1.remove();
    }

    var att = document.createAttribute("class");
    att.value = "animalList";
    table2.setAttributeNode(att);

    inner += `<tr>
                <th>length</th>
                <th>weight</th>
                <th>gender</th>
                <th>age</th>
              </tr>`;
    for(let animal of list) {
        inner += `<tr>
                    <td>${animal.length}</td>
                    <td>${animal.weight}</td>
                    <td>${animal.gender}</td>
                    <td>${animal.age}</td>
                   </tr>`
    }
    table2.innerHTML = inner;
    main.appendChild(table2)

    var deletebtn = document.getElementById('delete');
    deletebtn.setAttribute("onclick","addCheckbox()");
}

function birdExpander(elm) {
    var table = document.getElementsByClassName('menue')[0];
    var table2 = document.createElement('table');
    var inner = '';

    var att = document.createAttribute("class");
    att.value = "menue";
    table2.setAttributeNode(att);

    inner += '<tr><td onclick="showParrotList()">Parrot</td></tr>';
    inner += '<tr><td onclick="showPigeonList()">Pigeon</td></tr>';
    inner += '<tr><td onclick="showMynahList()">Mynah</td></tr>';
    table2.innerHTML = inner;
    table.parentNode.replaceChild(table2,table);
}
function showParrotList() {
    obj = new Parrot();
    selectedItem = parrotList;
    showList(parrotList);
}
function showPigeonList() {
    obj = new Pigeon();
    selectedItem = pigeonList;
    showList(pigeonList);
}
function showMynahList() {
    obj = new Mynah();
    selectedItem = mynahList;
    showList(mynahList);
}

function addCheckbox() {
    var table = document.getElementsByClassName('animalList')[0];
    if (table !== undefined) {
        var rows = document.querySelectorAll('.animalList tr');
        var btn = document.getElementById('delete');
        for(let i = 1 ; i < rows.length ; i++) {
            let td = document.createElement('td')
            td.innerHTML = `<input type="checkbox" data-index="${i}">`;
            rows[i].appendChild(td);
        }
        btn.setAttribute("onclick","deleteItems()");
    }
}

function deleteItems() {
    var delRows = document.querySelectorAll('input[type="checkbox"]');
    var delIndexArray = new Array(0);
    var newArray = new Array(0);

    for(let row of delRows) {
        if (row.checked === true) {
            delIndexArray.push(row.dataset.index-1);
        }
    }

    for(let i = 0 ; i < selectedItem.length ; i++) {
        if(delIndexArray.includes(i)) {
            continue;
        }
        newArray.push(selectedItem[i]);
    }
    selectedItem.length = 0;
    selectedItem.push.apply(selectedItem, newArray);
    //selectedItem.splice(0,selectedItem.length,newArray);
    showList(selectedItem);
}

function addForm() {
    var form = document.getElementsByClassName('form')[0];
    if (form.style.display === 'none' || form.style.display === '') {
        form.style.display = 'block';
    } else {
        form.style.display = 'none';
    }
    document.getElementById("addItem").addEventListener("submit", addItemForm);
}



function addItemForm(event) {
    event.preventDefault();
    var length = this["length"].value;
    var weight = this["weight"].value;
    var gender = this["gender"].value;
    var age = this["age"].value;

    obj.length = length;
    obj.weight = weight;
    obj.gender = gender;
    obj.age = age;
    selectedItem.push(obj);
    showList(selectedItem);
}
